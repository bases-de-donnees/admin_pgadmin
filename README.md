Outils d'administration pour les bases de données

## pgAdmin

pgAdmin est un outil d'administration pour PostgreSQL

## phpMyAdmin

pgAdmin est un outil d'administration pour MySQL/MariaDB

## Prérequis

Créer un réseau pour assurer la communication entre les outils et les bases à administrer.

```
docker network create webadmin_net
```
